#!/bin/bash
###############################################################################
# resize-sponsor-logo.sh - Resize sponsor image to fit
# jekyll-theme-centos-sponsor recommended image size.
#
# Copyright (c) 2024 Alain Reguera Delgado
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
###############################################################################

# Set desired image width, in pixels, for jekyll-theme-centos-sponsors
# presentation.
THEME_IMAGE_WIDTH=${THEME_IMAGE_WIDTH:-242}

# Set desired image height, in pixels, for jekyll-theme-centos-sponsors
# presentation.
THEME_IMAGE_HEIGHT=${THEME_IMAGE_HEIGHT:-60}

# Set absolute path to directory where sponsor logo images are stored.
# Consider, this script is executed inside a container image that mounts jekyll
# site directory structure under "/site" path.
SPONSOR_LOGO_DIR=${SPONSOR_LOGO_DIR:-/site/assets/img/sponsors}

# Create list of files to process. Currently, only PNG and JPG image types are
# supported.
SPONSOR_LOGO_FILES=$(find ${SPONSOR_LOGO_DIR} -type f -name '*.png' -o -name '*.jpg')

get_help() {
  cat <<EOF
${0}

Resize sponsor logo to fit jekyll-theme-centos-sponsor recommended image size
to ensure the sponsor logo image is presented with the right proportions in
https://www.centos.org/sponsors/. Images are resized only when they are larger
than recommended size for sponsor logo images. When an image is resized, its
ratio is preserved both in height and width, empty space created as consequence
of resizing is covered with transparent background. The resized sponsor logo
image is centered inside the recommended image size.

Usage:

  1. Resize images in "/site/assets/img/sponsors" directory:

    podman run --rm ${0} registry.gitlab.com/centos/artwork/centos-web/pipelines/toolbox/gm:latest \\
      -v </path/to/jekyll/site>:/site:z \\
      resize-sponsor-logo

  2. Resize images in a custom (e.g., /site/assets/img) directory:

    podman run --rm ${0} registry.gitlab.com/centos/artwork/centos-web/pipelines/toolbox/gm:latest \\
      -e SPONSOR_LOGO_DIR=/site/assets/img \\
      -v </path/to/jekyll/site>:/site:z \\
      resize-sponsor-logo

Contribute:
  - https://gitlab.com/CentOS/artwork/centos-web/pipelines/toolbox/-/blob/main/gm/resize-sponsor-logo.sh
EOF
}

get_image_width() {
  gm identify -format "%w" ${1}
}

get_image_height() {
  gm identify -format "%h" ${1}
}

resize_image_width() {
  local WIDTH=$(get_image_width ${1})
  echo "Resizing \"${1}\" image width from ${WIDTH}px to ${THEME_IMAGE_WIDTH}px..."

  gm convert ${1} -resize ${THEME_IMAGE_WIDTH}x -gravity center -background transparent -extent ${THEME_IMAGE_WIDTH}x${THEME_IMAGE_HEIGHT} ${1}.tmp
  mv ${1}.tmp ${1}
}

resize_image_height() {
  local HEIGHT=$(get_image_height ${1})
  echo "Resizing \"${1}\" image height from ${HEIGHT}px to ${THEME_IMAGE_HEIGHT}px..."

  gm convert ${1} -resize x${THEME_IMAGE_HEIGHT} -gravity center -background transparent -extent ${THEME_IMAGE_WIDTH}x${THEME_IMAGE_HEIGHT} ${1}.tmp
  mv ${1}.tmp ${1}
}

resize_sponsor_logo() {
  for SPONSOR_LOGO_FILE in ${SPONSOR_LOGO_FILES};do
    local SPONSOR_LOGO_WIDTH=$(get_image_width ${SPONSOR_LOGO_FILE})
    local SPONSOR_LOGO_HEIGHT=$(get_image_height ${SPONSOR_LOGO_FILE})

    if [[ ${SPONSOR_LOGO_WIDTH} -gt ${THEME_IMAGE_WIDTH} ]]; then
      resize_image_width ${SPONSOR_LOGO_FILE}
    fi

    if [[ ${SPONSOR_LOGO_HEIGHT} -gt ${THEME_IMAGE_HEIGHT} ]]; then
      resize_image_height ${SPONSOR_LOGO_FILE}
    fi
  done
}

case "$@" in
  -h|--help)
    get_help
  ;;
  *)
    resize_sponsor_logo
  ;;
esac
